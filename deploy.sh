#!/bin/bash
while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in
        -h|--host)
        shift
        HOST="$1"
        ;;
        -u|--user)
        shift
        USER="$1"
        ;;
        -p|--path)
        shift
        PATH="$1"
        ;;
        -op|--origin-path)
        shift
        ORIGIN_PATH="$1"
        ;;
        *)
        # Do whatever you want with extra options
        echo "Unknown option '$key'"
        ;;
    esac
    # Shift after checking all the cases to get the next option
    shift
done

apt-get update && apt-get install rsync -y

##Get servers list
set -f
string=${HOST}
array=(${string//,/ })

##Iterate servers for deploy and pull last commit
for i in "${!array[@]}"
do
    SERVER=${array[i]}
    echo "Iniciando deploy | Host ${SERVER}"
    rsync -zrSlhv --stats --exclude-from=deployment-exclude-list.txt ${ORIGIN_PATH}/ ${USER}@${SERVER}:${PATH}
done
